package com.example.com.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by harshita30 on 2/6/16.
 */
public class IncomingSms extends BroadcastReceiver {

    final SmsManager smsManager = SmsManager.getDefault();
    private SmsMessage smsMessage;
    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();

        try {
            if (bundle != null) {
                Object[] pdus = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdus.length; i++) {
                    smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    String senderNumber = smsMessage.getDisplayOriginatingAddress();
                    String message = smsMessage.getDisplayMessageBody();

                    Intent dataIntent = new Intent(context,SendDataToServer.class);
                    dataIntent.putExtra("userId",1);
                    dataIntent.putExtra("message",message);
                    dataIntent.putExtra("senderNum",senderNumber);
                    context.startService(dataIntent);
                    Log.i("SmsReceiver", "senderNum: " + senderNumber + "; message: " + message+ i);
                    Toast.makeText(context, "senderNum: " + senderNumber + "; message: " + message, Toast.LENGTH_LONG).show();
                }
            }
        }
            catch (Exception e) {
                Log.e("SmsReceiver", "Exception smsReceiver" +e);

            }

    }
}
