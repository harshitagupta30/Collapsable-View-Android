package com.example.com.myapplication.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.com.myapplication.Domain;
import com.example.com.myapplication.R;
import com.example.com.myapplication.SuggestionAdapter;

/**
 * Created by harshita30 on 1/6/16.
 */
public class PageFragment extends Fragment {
    private ListView suggestionsList;
    private SuggestionAdapter suggestionAdapter;

    public static PageFragment newInstance(Domain domain) {
        PageFragment fragment = new PageFragment();
        Bundle bdl = new Bundle();
        bdl.putSerializable("domainKey",domain);
        fragment.setArguments(bdl);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Domain domain = (Domain) getArguments().getSerializable("domainKey");
        View view = inflater.inflate(R.layout.fragment_faq, container, false);
        suggestionsList =(ListView) view.findViewById(R.id.faqList);
        suggestionAdapter = new SuggestionAdapter(getActivity(), domain.getChatSuggestion());
        suggestionsList.setAdapter(suggestionAdapter);
        return view;
    }
}
