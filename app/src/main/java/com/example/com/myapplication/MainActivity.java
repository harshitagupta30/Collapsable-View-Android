package com.example.com.myapplication;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {
    private RelativeLayout relativeLayoutTabs;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private EditText edtTxtInputMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        relativeLayoutTabs= (RelativeLayout) findViewById(R.id.relativeLayoutTabs);
        relativeLayoutTabs.setVisibility(View.GONE);
        edtTxtInputMessage =(EditText) findViewById(R.id.edtTxtInputMessage);
        viewPager = (ViewPager) findViewById(R.id.chatOptionsPager);
        tabLayout = (TabLayout) findViewById(R.id.chatBottomTabs);
        Domain d = new Domain();
        viewPager.setAdapter(new SampleFragmentAdapter(getSupportFragmentManager(),
                MainActivity.this,d.getDomainsList()));
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.cab);
        tabLayout.getTabAt(1).setIcon(R.drawable.utilities);
        tabLayout.getTabAt(2).setIcon(R.drawable.burger);
        tabLayout.getTabAt(3).setIcon(R.drawable.recharge);
        tabLayout.getTabAt(4).setIcon(R.drawable.bus);
//        state = relativeLayoutTabs.isShown();
//        Log.d("LayoutState",String.valueOf(state));
    }

    @Override
    public void onBackPressed() {
        if (relativeLayoutTabs.isShown()) {
            relativeLayoutTabs.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    private void hideKeyboard() {
        try {
            if (edtTxtInputMessage != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtTxtInputMessage.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void toggle_contents(View v) {
        hideKeyboard();
//        state = relativeLayoutTabs.isShown();
//        Log.d("LayoutState",String.valueOf(state));
        if (relativeLayoutTabs.isShown()) {
            relativeLayoutTabs.setVisibility(View.GONE);
            slide_up(this,relativeLayoutTabs);

        } else {
            slide_down(this,relativeLayoutTabs);
            relativeLayoutTabs.setVisibility(View.VISIBLE);

        }
    }


    public static void slide_down(Context context, View v) {

        Animation a = AnimationUtils.loadAnimation(context, R.anim.slide_down);
        if (a != null) {
            a.reset();
            if (v != null) {
                v.clearAnimation();
                v.startAnimation(a);
            }
        }
    }

    public static void slide_up(Context context, View v) {

        Animation a = AnimationUtils.loadAnimation(context, R.anim.slide_up);
        if (a != null) {
            a.reset();
            if (v != null) {
                v.clearAnimation();
                v.startAnimation(a);
            }
        }
    }
}
