package com.example.com.myapplication;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by harshita30 on 2/6/16.
 */
public class SendDataToServer extends IntentService {

    private static final String TAG = "SendDataToSeverService";
    private static final String url= "http://beta-api.niki.ai/user/sms";
    private Intent intent;
    JSONObject response = null;

    @Override
    public void onStart(Intent intent, int startId) {
        this.intent = intent;
    }

    public SendDataToServer(){
        super(SendDataToServer.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG,"Service Started");

        if (!TextUtils.isEmpty(url)){
            try {
                uploadData(url);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private JSONObject parseData(){
        String message = intent.getStringExtra("message");
        String userId = intent.getStringExtra("userId");
        String senderNum = intent.getStringExtra("senderNum");
        try{
            response = new JSONObject();
            response.put("message",message);
            response.put("userId",userId);
            response.put("senderNum",senderNum);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }
    private void uploadData(String requesturl) throws IOException{
        HttpURLConnection httpURLConnection = null;
        URL url = new URL(requesturl);
        httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod("POST");
        int statusCode = httpURLConnection.getResponseCode();

        if (statusCode == 200){
            httpURLConnection.connect();
            OutputStreamWriter wr = new OutputStreamWriter(httpURLConnection.getOutputStream());
            wr.write(response.toString());
            wr.flush();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String s = "";
            StringBuilder stringBuilder = new StringBuilder("");
            while ((s = bufferedReader.readLine()) != null) {
                stringBuilder.append(s);
            }
            String serverResponseMessage = stringBuilder.toString();
            Log.d(TAG,serverResponseMessage);
        }

    }
}
