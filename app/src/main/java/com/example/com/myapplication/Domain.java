package com.example.com.myapplication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sitaram on 4/29/16.
 */
public class Domain implements Serializable {
    private String merchantId;
    private boolean review;
    private String logo;
    private String time;
    private boolean locationRequired;
    private String facebook;
    private String beta;
    private String support_email;
    private String twitter;
    private String name;
    private String domain;
    private String support_phone;
    private String status;
    private List<String> chatSuggestion;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public boolean isReview() {
        return review;
    }

    public void setReview(boolean review) {
        this.review = review;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isLocationRequired() {
        return locationRequired;
    }

    public void setLocationRequired(boolean locationRequired) {
        this.locationRequired = locationRequired;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getBeta() {
        return beta;
    }

    public void setBeta(String beta) {
        this.beta = beta;
    }

    public String getSupport_email() {
        return support_email;
    }

    public void setSupport_email(String support_email) {
        this.support_email = support_email;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getSupport_phone() {
        return support_phone;
    }

    public void setSupport_phone(String support_phone) {
        this.support_phone = support_phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getChatSuggestion() {
        return chatSuggestion;
    }

    public void setChatSuggestion(List<String> chatSuggestion) {
        this.chatSuggestion = chatSuggestion;
    }


    public List<Domain> getDomainsList() {
        List<Domain> domains = new ArrayList<>();
        Domain domain = new Domain();
        domain.setDomain("Cab");
        List<String> suggestions = new ArrayList<>();
        suggestions.add("Cab");
        suggestions.add("Book the cheapest cab");
        suggestions.add("Book the nearest cab");
        suggestions.add("Book an ola mini");
        suggestions.add("Book a sedan");

        domain.setChatSuggestion(suggestions);
        domains.add(domain);

        domain = new Domain();
        domain.setDomain("Utility");
        suggestions = new ArrayList<>();
        suggestions.add("Pay my electricity bill");
        suggestions.add("Pay my postpaid mobile bill");
        suggestions.add("Pay my dth bill");
        suggestions.add("Pay my datacard bill");
        domain.setChatSuggestion(suggestions);
        domains.add(domain);

        domain = new Domain();
        domain.setDomain("Food");
        suggestions = new ArrayList<>();
        suggestions.add("I need to order burger");
        suggestions.add("I need chicken burger combo");
        suggestions.add("I need a veg burger combo");
        suggestions.add("Can you deliver burger?");
        domain.setChatSuggestion(suggestions);
        domains.add(domain);


        domain = new Domain();
        domain.setDomain("Recharge");
        suggestions = new ArrayList<>();
        suggestions.add("Recharge my phone");
        suggestions.add("I need a 3g data recharge");
        suggestions.add("I need a full talktime plan");
        suggestions.add("I need a 2g recharge");
        domain.setChatSuggestion(suggestions);
        domains.add(domain);

        domain = new Domain();
        domain.setDomain("Bus");
        suggestions = new ArrayList<>();
        suggestions.add("Book a bus for me");
        suggestions.add("I need to book a bus");
        suggestions.add("I need to book a bus for today");
        suggestions.add("I need to book a bus for tomorrow");
        domain.setChatSuggestion(suggestions);
        domains.add(domain);

        return domains;
    }
}
