package com.example.com.myapplication;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.com.myapplication.Fragments.PageFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harshita30 on 1/6/16.
 */
public class SampleFragmentAdapter extends FragmentPagerAdapter {
    private Context context;
    List<Domain> domainList;
    List<Fragment> fragmentList;

    public SampleFragmentAdapter(FragmentManager fm, Context context,List<Domain> domains) {
        super(fm);
        this.context = context;
        domainList = domains;
        fragmentList = new ArrayList<>();
        for (Domain domain : domains){
            fragmentList.add(PageFragment.newInstance(domain));
        }
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }
}
