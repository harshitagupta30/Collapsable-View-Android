package com.example.com.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by sitaram on 4/29/16.
 */
public class SuggestionAdapter extends BaseAdapter {
    private Context context;
    private final LayoutInflater layoutInflater;
    private List<String> suggestions;
    private static QuickSuggestionListener listener;

    /*public void setListener(QuickSuggestionListener listener) {
        SuggestionAdapter.listener = listener;
    }*/

    public SuggestionAdapter(Context context, List<String> suggestions) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.suggestions = suggestions;
    }

    @Override
    public String getItem(int position) {
        return suggestions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getCount() {
        return suggestions.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder viewHolder;

        if (view == null) {
            viewHolder = new ViewHolder();
            view = layoutInflater.inflate(R.layout.item_chat_suggestion, parent, false);
            viewHolder.tvSuggestion = (TextView) view.findViewById(R.id.suggestionText);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        final String suggestion = suggestions.get(position);
        if (suggestion != null) {
            viewHolder.tvSuggestion.setText(suggestion);

            viewHolder.tvSuggestion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onSelected(suggestion);
                    }
                }
            });
        }


        return view;
    }


    private static class ViewHolder {
        protected TextView tvSuggestion;

    }

    public interface QuickSuggestionListener {
        void onSelected(String suggestion);
    }
}
